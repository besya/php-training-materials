<?php

/*
 * Скалярные
 *  
 * boolean
 * integer 2
 * float 
 * string
 *
 */

/*
 * Смешанные
 *  
 * array
 * object
 *
 */

/*
 * Специальные
 *  
 * resource
 * null
 *
 */


$a = 80;
$b = 90;

// В 80x и 90х годах

//echo "\t Hello \"World\" ";

$string1 = 'В ' . $a . 'х и ' . $b . 'х годах';
$string2 = "В {$a}x и {$b}x годах ";
//echo $string2;

// Оператор склеивания

//$separator = PHP_EOL;

$firstName  = "Igor";
$lastName   = "Bespalov";

$fullName = $firstName . ' - ' . $lastName;

// Комбинированные операторы

$a = 5;

$a = $a + 5;
$a += 5;

// +=, -=, *=, /=, %=, .=

$test = 'Первая часть';
$test .= ' Вторая часть';

//echo $test;


// Тип null
/*
 * Переменная считается null, если:
 * 1) ей была присвоена константа null
 * 2) ей еще не было присвоено никакого значения
 * 3) она была удалена с помощью unset()
 * 
 */

//echo gettype($variable); 

//$c = 5.2;
//if(is_numeric($c)) echo 'yes';


// Приведение типов
// integer, boolean, float, string, object, unset (null)

$a = '5 человек';
$a = (integer)$a;
//echo $a;

// Неявное преобразование

$a = '7';
$b = 10;
$c = $a + $b; // $c - integer
//echo $c;


// Arrays

$array1 = array('Red', 'Blue', 'White');
$array2 = array('firstName' => 'Igor', 'middleName' => 'Bespalov');
$array1[0] = 'Black';

unset($array1[0]);

echo '<pre>';
print_r($array1);
echo '</pre>';

//  var_dump($array2);


$array2['lastName'] = 'Ivanovich';

//echo count($array2);

$altArray = [
    [1,2,3,4],
    'asd' => [1,2,3,4,[3,2,3]]
];

sort($altArray);

$array3 = array(
    array(1,2,3,4,5),
    array(array('a', 'b', 'c', 'd'), 'b', 'c', 'd'),
    'array3' => array('a', 'b', 'c', 'd')
);

//print_r($array3);

foreach($array2 as $key => $value){
    $array2[$key] = $newValue;
}