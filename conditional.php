<?php

$a = 'человек';
$b = -8;

//var_dump((integer)$a);

//var_dump($a < $b);

//exit;

//$a = 1/0;

if ($a == $b) {
    echo 'a == b';
} elseif ($a > $b) {
    echo 'a > b';
} else {
    echo 'a < b';  
}

//if (!$myVariable) echo 123;


// При преобразовании в boolean, рассматриваются как FALSE:
/*
 * 
 * 1) само значение FALSE
 * 2) integer 0
 * 3) float 0.0
 * 4) пустая строка и строка "0"
 * 5) массив без элементов
 * 6) особый тип NULL
 * 
 */

// Все остальные значения рассматриваются как TRUE


// switch-case

$day = 2;

switch ($day) {
    case 1: echo 'Понедельник'; break;
    case 2: echo 'Вторник';     break;
    case 3: echo 'Среда';       break;
    case 4: echo 'Четверг';     break;
    case 5: echo 'Пятница';     break;
    default : echo 'Выходной';
}



// Тернарный оператор

$status = true;

if ($status && (true && false)) {
    echo 'Активен';
} else {
    echo 'Неактивен';
}

echo ($status) ? "Активен" : "Неактивен";



$f = (1 + 4) / (2 * 3);
