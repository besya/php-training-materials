<?php

function ref_reverse(&$arr){
   $arr = array_reverse($arr, true);
}

$arr = [1,2,3,4,5,6,7,8,9,10];

//$arr2 = array_reverse($arr, true);

//ref_reverse($arr);

//print_r($arr);

// Функции для работы с переменными

/*
 * isset
 * empty
 * gettype
 * is_*
 * get_defined_vars()
 * 
 */

//$me = null;
//var_dump();
//echo '<pre>';
//print_r(get_);
//echo '</pre>';


// Обработка строк

$city = 'Лондон';
$percent = 22 / 41 * 100;
$total = 1000;

//echo $percent;

//$format = 'По данным статистики %.2f из %d  %1$.2f \n\r опрошенных... в городе %s';

//printf($format, $percent, $total, $city);

//$result = sprintf($format, $percent, $total, $city);

/*
 * strlen
 * substr
 * str_replace
 * explode
 * implode
 * 
 */

$s = 'Привет Игорь';

//$a = split(' ', $s);

//$s2 = join('-', $a);

//print_r($s2);

//echo str_replace('234', '', $s);

// Математические функции
/*
 * max
 * min
 * rand
 * round
 * ceil
 * floor
 * 
 */

$arr = [1,2,3,4];
$f = 4.999023456;

//echo floor($f);

//echo rand();

// Работа с массивами

/*
 * in_array
 * array_values
 * array_keys
 * array_intersect
 * array_diff
 * 
 */

//$arr1 = [1,2,3,4,5];
//$arr2 = [4,5,6,7,8];

$arr1 = ['Igor'];

//list($name) = $arr1;

//echo $name . ' ' . $age;

//print_r(array_keys($arr1));

// Сортировка массивов

/*
 * sort
 * ksort
 * 
 */

// Дата и время

/*
 * 
 */

$now = getdate();
//print_r($now);
$timestamp = time();
//echo time();
//mktime($total, $minute, $second, $month, $day, $year, $is_dst)



// Форматирование даты

//echo date("Y-m-d H:i:s", time());

// Языковые конструкции

/*
 * 
 * die & exit
 * echo & print
 * isset & unset
 * include & include_once
 * require & require_once
 * empty
 * eval
 * list
 * 
 */

require '';

$a = 10;
$b = 5;

$code = '$c = $a + $b;';

eval($code);

echo $c;

