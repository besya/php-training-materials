<?php

// Пользовательские функции

function myFirstFunction(){
    echo '<h1>Hello world</h1>';
}

//myfirstfunction();

//Правила использования:
/*
 * 1) Правила для имен функций такие же как и для переменных.
 * 2) Имя функции не зависит от регистра
 * 3) Функцию нельзя декларировать 2 раза. Это делается всего 1 раз.
 * 4) Функции необязательно определять до использования.
 * 
 */


// Аргументы функции

function greeting($name){
    echo "<h2>Hello, $name<h2>";
}

//greeting('Igor');
//greeting();

$names = ['Jim', 'Alex', 'Laura'];

//foreach($names as $name) greeting($name);

function massGreeting($names){
    foreach($names as $name) echo "Hello, $name<br/>";
}

//massGreeting([1,2,3]);
//massGreeting();
//echo 123;

// Области видимости переменных
/*
 * 1) Локальная область видимости
 * 2) Глобальная область видимости
 */

$a = 1;

function myFunction($name = 'Igor'){
    $a = 2;
}

//myFunction();
//var_dump($a);
//var_dump($name);


//Доступ к глобальным переменным внутри функции
/*
 * 1) При помощи global
 * 2) Используя специальный массив $GLOBALS
 * 
 */

$name1 = 'Tea';
$name2 = 'Coffee';

function testGlobals(){
    $name3 = 'Juice';
    greeting($name3);
    
    // #1
    global $name1;
    greeting($name1);
    
    // #2
    greeting($GLOBALS['name2']);
    
    //$GLOBALS['name2'] = 'Water';
}

//testGlobals();
//greeting($name2);


// Передача аргументов по ссылке

function test(&$beta){
    $beta = 5;
}

$beta = 10;
//test($beta);
//test(13);

//echo $beta; // 5 or 10?


// Статические переменные

function howManyTimes(){
    $a = 0;
    echo ++$a;
}

//howManyTimes();
//howManyTimes();
//howManyTimes();


// Возврат значений

function perimetr($a, $b){
    $p = (2 * $a) + (2 * $b);
    return $p;
}

$a = 10;
$b = 20;

$p = perimetr($a, $b);

//echo $p;


// Рекурсивная функция

$catalog = [
    'Автотехника' => [
        'Вело' => [
            'Велосипеды' => [
                'Extreme'   => 10,
                'Author'    => 4
            ],
            'Самокаты' => 12
        ],
        'Авто' => [
            'Opel' => [
                'Vivaro' => 5
            ],
            'Honda' => 10,
            'Audi'  => 11
        ],
        'Аксессуары' => 7
    ],
    'Фототехника' => [
        'Фотокамеры'    => 6,
        'Видеокамеры'   => 3
    ],
    'Другое' => 3
];


// var_dump(is_array($catalog));
// var_dump(is_array(55));

function recursiveCount($element){
    $count = 0;
    
    if(is_array($element)){
        foreach ($element as $elm){
            $count += recursiveCount($elm);
        }
    }else{
        $count += $element;
    }
    
    return $count;
}

//echo recursiveCount([]);

$arr = [1,2,3,4,5];

//array_walk($arr, function(&$item){ $item++; });

//print_r($arr);

$discount = 6;

$getDiscount = function($price) use (&$discount) {
    $discount++;
    return $price - $discount;
};

$discount = 5;

//echo $getDiscount(10);



$a = 10;

$fn = function &() use (&$a){
    $b = $a + 1;
    return $a;
};

$r = &$fn();

$r = 2;

//echo $a;
